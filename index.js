require('kohanajs').addNodeModule(__dirname);

module.exports = {
  LiquidView: require('./classes/LiquidView'),
  LiquidTags: require('./classes/LiquidTags'),
  LiquidFilters: require('./classes/LiquidFilters'),
  LiquidTagsForm: require('./classes/liquid-tags/Form'),
  LiquidTagsPaginate: require('./classes/liquid-tags/Paginate'),
  LiquidTagsSchema: require('./classes/liquid-tags/Schema'),
  LiquidTagsSection: require('./classes/liquid-tags/Section'),
  LiquidTagsStub: require('./classes/liquid-tags/Stub'),
  LiquidTagsStyle: require('./classes/liquid-tags/Style'),
  LiquidTagsTag: require('./classes/liquid-tags/Tag'),
  LiquidHelperConfig: require('./classes/helpers/Config'),
  LiquidHelperLiquid: require('./classes/helpers/Liquid'),
};
