/**
 *
 * tagToken:

 "trimLeft":true,
 "trimRight":true,
 "type" : "tag",
 "raw" : "{%- form 'product', _product, class : form_classes, novalidate: 'novalidate' -%}",
 "value" : "form 'product', _product, class : form_classes, novalidate: 'novalidate'",
 "input" : ,
 "file" : "xxx.liquid",
 "name" : "form",
 "args" : "'product', _product, class : form_classes, novalidate: 'novalidate'"

 */

module.exports = {
  form: require('./liquid-tags/Form'),
  paginate: require('./liquid-tags/Paginate'),
  stub: require('./liquid-tags/Stub'),
  tag: require('./liquid-tags/Tag'),
  Style: require('./liquid-tags/Style'),
};
