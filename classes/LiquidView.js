const fs = require('fs');
const path = require('path');
const { View } = require('@kohanajs/core-mvc');
const { KohanaJS } = require('kohanajs');
const { Liquid } = require('liquidjs');

const HelperConfig = require('./helpers/Config');
const HelperLiquid = require('./helpers/Liquid');

class LiquidView extends View {
  realPath = "";

  constructor(file, data = {}, themeDirectory = null) {
    super(`${file}.liquid`, data);

    let themePath = themeDirectory;
    // load settings
    const settings = HelperConfig.loadSettings(themePath, this.sectionFile);
    Object.assign(this.data, { settings: settings.current });

    if (!themePath) {
      const fetchedView = KohanaJS.resolveView(this.file);

      // file can be full path or partial path in KohanaJS View folder
      // match views folder,
      if (/[\\/]views[\\/]/i.test(fetchedView)) {
        themePath = path.normalize(fetchedView.replace(/[\\/]views[\\/].+$/, '/views'));
      } else {
        themePath = path.normalize(path.dirname(fetchedView));
      }

      this.realPath = fetchedView;
    } else {
      this.realPath = path.normalize(`${themePath}/${this.file}`);
      KohanaJS.viewPath.set(this.realPath, this.realPath);
    }

    if (!View.caches[this.realPath]) {
      View.caches[this.realPath] = this.setupLiquidEngine(themePath);
    }
  }

  setupLiquidEngine(themePath) {
    const engine = new Liquid({
      root: [`${themePath}/snippets`, `${themePath}/templates`],
      extname: '.liquid',
      cache: !!KohanaJS.config.view?.cache,
      globals: this.data,
    });

    HelperLiquid.registerFilterTags(engine);

    return {
      engine,
      template: engine.parse(fs.readFileSync(this.realPath, 'utf8')),
    }
  }

  async render() {
    const { engine, template } = ( KohanaJS.config.view.cache) ? (View.caches[this.realPath] || this.setupLiquidEngine(KohanaJS.VIEW_PATH)) : this.setupLiquidEngine(KohanaJS.VIEW_PATH);
    return engine.render(template, this.data);
  }
}

module.exports = LiquidView;
