const { KohanaJS } = require('kohanajs');

class HelperLiquid {
  static registerFilterTags(engine){
    if(!KohanaJS.config.liquidjs) return;
    KohanaJS.config.liquidjs.filters.forEach(filter => engine.registerFilter(filter.name, filter.func));
    KohanaJS.config.liquidjs.tags.forEach(tag =>engine.registerTag(tag.name, tag.tag));
  }
}

module.exports = HelperLiquid;
