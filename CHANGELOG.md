# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.6.4](https://gitlab.com/kohanajs-modules/liquid-vew/compare/v3.6.2...v3.6.4) (2023-12-03)


### Bug Fixes

* missing LiquidView.cache ([1b5b949](https://gitlab.com/kohanajs-modules/liquid-vew/commit/1b5b9495694af6e4091d466d93e2efd286ac76dc))

### [3.6.3](https://gitlab.com/kohanajs-modules/liquid-vew/compare/v3.6.2...v3.6.3) (2023-03-15)

### [3.6.2](https://gitlab.com/kohanajs-modules/liquid-vew/compare/v3.6.1...v3.6.2) (2023-02-20)

### [3.6.1](https://gitlab.com/kohanajs-modules/liquid-vew/compare/v3.6.0...v3.6.1) (2022-06-22)

## [3.6.0](https://gitlab.com/kohanajs-modules/liquid-vew/compare/v3.5.7...v3.6.0) (2022-06-15)


### Features

* search render snippets from templates folder ([3da4f0d](https://gitlab.com/kohanajs-modules/liquid-vew/commit/3da4f0deb4c9fa1ef7a563ef0999c30a2104ef67))

### 3.5.7 (2022-03-09)

### [3.5.6](https://gitlab.com/kohanajs-modules/liquid-vew/compare/v3.5.5...v3.5.6) (2022-03-09)

### [3.5.5](https://gitlab.com/kohanajs-modules/liquid-vew/compare/v3.5.4...v3.5.5) (2021-10-29)

### 3.5.4 (2021-10-19)

## [3.5.12 - 2021-10-13
### Changed
- use liquidjs@9.27.1

## [3.5.1] - 2021-10-13
### Changed
- npm module liquidjs

## [3.5.0] - 2021-09-09
### Added
- npm module liquidjs

## [3.4.1] - 2021-09-07
### Added
- create CHANGELOG.md